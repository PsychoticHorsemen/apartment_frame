var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

/// Schema for the Apartment we will be working with.
var dataSchema = new Schema({
    timestamp:String,
    data:String
}, { _id : false });

var sensorInfoSchema = new Schema({
    type:String,
    date:String,
    placement:String
}, { _id : false })

var sensorSchema = new Schema({
    sensorID:String,
    sensorInfo: sensorInfoSchema,
    data:[dataSchema]
}, { _id : false });

var occupantsSchema = new Schema({
    name:String,
    date:String
}, { _id : false });

var apartmentSchema = new Schema({
    info: {
        size:String,
        rooms:String,
        address:String
    },
    tcp_address:{
        ip:String,
        port:String
    },
    occupants:[occupantsSchema],
    sensors:[sensorSchema]
});

module.exports = mongoose.model("apartment", apartmentSchema);