var ip = require('ip');
var public_ip = require('public-ip');
var Apartments = require("./Apartments");

/// We define the standard function for creating a new apartment. Quicker and easier to work with in App.js
exports.createNewApartment = function(size, rooms, address, occupants, prt) {
    var apartment = new Apartments({
        info: {
            size:       size,
            rooms:      rooms,
            address:    address
        },
        tcp_address: {
            ip:         ip.address(),
            port:       prt
        },
        occupants:      occupants,
        sensors:        []
    });
    return apartment;
};

/// This function adds sensors to our apartment.
exports.addSensorToApartment = function(apartment, id, type, date, placement) {


    var newSensor = {
        sensorID: id,
        sensorInfo: {
            type: type,
            date: date,
            placement: placement
        },
        data: []
    };


    /// We find the apartment by searching for its ID, then we use the $set operator, to
    /// add the object into the array.
    Apartments.findByIdAndUpdate(apartment._id, {
        $push: {
            sensors: newSensor
        }
    }, function(error, doc) {
        if(error){
            console.log(error);
            // console.log("Error");
        }
        if(doc) {
            Apartments.findById(apartment._id, function (error, doc) {
                apartment = doc;
                console.log("Added sensors to apartment [" + apartment._id + "]");

                return apartment;
            });
        }
    });
};

/// Update TCP information for Apartment
exports.UpdateTCP = function (id, nIP, nPort) {

    Apartments.findByIdAndUpdate(id, {
        $set: {
            tcp_address: {
                ip: nIP,
                port: nPort
            }
        }
    }, function (error, doc) {
        return doc
    });
};