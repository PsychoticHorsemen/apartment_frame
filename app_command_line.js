/////   REQUIRE     ///////////////////////////////////////////////////////////////////////////////////////////////////
var express = require("express");
var morgan = require("morgan");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var app = express();
var ip = require('ip');
var publicIp = require('public-ip');

/////   DEFINE     ////////////////////////////////////////////////////////////////////////////////////////////////////
app.use(morgan("tiny"));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(express.static("public"));

/////   COMMAND LINE     //////////////////////////////////////////////////////////////////////////////////////////////
var port = 8082;
var apartment = undefined;
var address = "_TEST_VEJ_03";
/**
 * This IF-statement handles the probable command line arguments passed to the application.
 * It is made specifically for testing continous run.
 * These lines of code might be commented in the case of running this on Heroku or WebStorm.
 */
if(process.argv.length > 2) {
    var args = process.argv;
    args.forEach((val, index) => {
        // console.log(`${index}: ${val}`);

        if(val === "-port") {
            port = parseInt(args[index+1]);
        }
        if(val === "-address") {
            address = args[index+1];
        }

    });

    if(address === undefined) {
        address = "_TEST_VEJ_02";
    }

} else {
    port = process.env.PORT || 8083;
}

console.log("Parameters: [" + port + "," + address + "]");

/////   DATABASE CONNECTION     ///////////////////////////////////////////////////////////////////////////////////////
var dbURI = "mongodb://root:toor@ds155299.mlab.com:55299/association_database";
// var dbURI = "mongodb://localhost:27017/association_database";

mongoose.connect(dbURI, function() {
    console.log("Connected to database");

    /// After the connection has been made, we will go through the process of creating the apartment-simulator-document
    /// on the MongoDB server.

    var size = "100";
    var rooms = "10";
    var occupants = [{"name": "Yu Yu", "date": "20-07-2011"}];
    // var occupants = undefined;

    /// We check if the apartment we are creating already exists. If no, then it gets created, else we retrieve the
    /// existing one and work with that.
    Apartments.find({"info.address": address }, function(error, doc) {

        if(doc.length === 1) {

            console.log("Apartment exists...");
            apartment = doc[0];
            console.log("ID: " + apartment._id);
            ApartmentController.UpdateTCP(apartment._id, ip.address(), port);

            /// We create a preset number of sensors for this apartment, and add them to its list.
            CreateSensors(false);

        } else {
            console.log("Creating apartment...");

            /// We create a new apartment, using a function existing in Controller.js
            apartment = ApartmentController.createNewApartment(size, rooms, address, occupants, port);

            console.log("ID: " + apartment._id);

            /// We save the newly created apartment.
            apartment.save().then(function () {
                console.log("Done...\n");

                /// We create a preset number of sensors for this apartment, and add them to its list.
                CreateSensors(true);

            }).catch(function (error) {
                console.log(error);
            });
        }
    });

});

/////   MODEL       ///////////////////////////////////////////////////////////////////////////////////////////////////
var Apartments = require("./Apartments");
var ApartmentController = require("./controller");

/////   SENSOR MODEL     //////////////////////////////////////////////////////////////////////////////////////////////

/**
 * A sensor constructor
 * @param name = Name of the sensor. fx. "LivingroomTemperature_01"
 * @param type = Type of the currentData collected. fx. "Temperature", "Moisture" or "Noise" etc.
 * @param unit = What unit of measurement the sensor uses. fx.: "Celsius", "%" or "Decibel", etc.
 * @param id = ID that is hardcoded from the sensor.
 * @param placement = placement
 * @constructor
 */

var sensorList = [];

var Sensor = function (name, type, unit, sensorId, placement) {
    this.name = name;
    this.type = type;
    this.unit = unit;
    this.sensorId = sensorId;
    this.placement = placement;
    //this.currentTimestamps = [];
    this.currentData = [];

    // same as timestamps and currentData. But currentData in these do not get removed.
    //this.storedTimestamps = [];
    this.storedData = [];

    this.placement = "undefined";
    that = this;
};


var CreateSensors = function (exists) {
    CreateNewSensor(exists, "sens_id_1", "Temperature", "20-10-2010", "Rum 1");
    // CreateNewSensor(exists, "sens_id_2", "Moisture", "20-10-2010", "Rum 2");
    // CreateNewSensor(exists, "sens_id_3", "Noise", "20-10-2010", "Rum 3");

    // CreateNewSensor(exists, "sens_id_4", "CO2", "20-10-2010", "Rum 4");
    // CreateNewSensor(exists, "sens_id_5", "psi", "20-10-2010", "Rum 5");
    // CreateNewSensor(exists, "sens_id_6", "Energy", "20-10-2010", "Rum 6");

    // CreateNewSensor(exists, "sens_id_7", "Water", "20-10-2010", "Rum 7");
    // CreateNewSensor(exists, "sens_id_8", "Oxygen", "20-10-2010", "Rum 8");
    // CreateNewSensor(exists, "sens_id_9", "Temperature 2", "20-10-2010", "Rum 9");

    // CreateNewSensor(exists, "sens_id_10", "Temperature 3", "20-10-2010", "Rum 10");
    // CreateNewSensor(exists, "sens_id_11", "Water 2", "20-10-2010", "Rum 11");
    // CreateNewSensor(exists, "sens_id_12", "Water 3", "20-10-2010", "Rum 12");

    console.log("Sensors created...\n");
};

var CreateNewSensor = function (exists, id, type, date, placement) {

    var sensor = new Sensor(undefined, type, undefined, id);
    sensorList.push(sensor);
    if(exists) {
        ApartmentController.addSensorToApartment(apartment, id, type, date, placement);
    }
};



/////   DATA GENERATION SIMULATION  ///////////////////////////////////////////////////////////////////////////////////

/**
 * A random number generator for simulating currentData collected by the sensors.
 * At a 10000ms interval, each sensor gets a random number from 1-100
 */
setInterval(dataLoop, 5000, 'dataLoop');
var tick = 0;
function dataLoop(arg) {
    console.log("Tick " + (tick++));
    sensorList.forEach(function (sensor) {

        var data = Math.floor(Math.random() * 100);
        var timestamp = Date.now();


        sensor.storedData.push( {
            "timestamp": new Date(timestamp).toLocaleString()
            ,"data":data.toString()
        });
        sensor.currentData.push( {
            "timestamp": new Date(timestamp).toLocaleString(),
            "data":data.toString()
        });

        console.log(sensor.sensorId);
    });
    console.log();
}

app.get("/data", function(req,res){

    var SendData = [];

    sensorList.map(function (sensor) {
        SendData.push({
            "sensorId":sensor.sensorId,
            "type":sensor.type,
            "name":sensor.name,
            "data":sensor.storedData
        });
        console.log(sensor.sensorId+"  "+sensor.type+"  "+sensor.name);
        console.log(sensor.storedData);

    });

    res.json(SendData);

});

/**
 * Pulls data from currentData for all sensors.
 */
app.get("/data/current", function (req, res) {
    var SendData = [];

    sensorList.map(function (sensor) {
        SendData.push({
            "sensorId":sensor.sensorId,
            "type":sensor.type,
            "name":sensor.name,
            "data":sensor.currentData
        });
        console.log(sensor.sensorId+"  "+sensor.type+"  "+sensor.name);
        console.log(sensor.currentData);
        sensor.currentData = [];
    });

    res.json(SendData)

});

/////   RUN     ///////////////////////////////////////////////////////////////////////////////////////////////////////
console.log("Local ip: " + ip.address());
// publicIp.v4().then(ip => {
//    console.log("Public IP: " + ip)
// });
module.exports = app;
app.listen(port);
console.log("Listening on port " + port);
