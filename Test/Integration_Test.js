var mongoose = require("mongoose");
var request = require("supertest");
var should = require("should");
// var app = require("../app");
var Apartments = require("../Apartments");
var controller = require("../controller");

var dbURI = "mongodb://root:toor@ds155299.mlab.com:55299/association_database";
// var dbURI = "mongodb://localhost:27017/association_database";
var TEST_ID = undefined;


// Denne before() er også en test i sig selv (specifik nr. 3 for User-story 22), da
// koden her vil blive delvis anvendt til selve oprettelsen af lejeligheden.
before("Opret Test lejlighed", function (done) {

    mongoose.connect(dbURI, function () {

        var apartment = new Apartments({
            info: {
                size: "100",
                rooms: "1",
                address: "_ACCEPT_TEST_"
            },
            tcp_address: {
                ip: "127.0.0.1",
                port: "8080"
            },
            occupants: [{
                name: "Test-Manson",
                date: "2018-05-01"
            }],
            sensors: [{
                sensorID: "Sensor_01",
                sensorInfo: {
                    type: "Temperature",
                    date: "2018-05-01",
                    placement: "1"
                },
                data: []
            }]
        });

        apartment.save().then(function () {
            TEST_ID = apartment._id;
            console.log("Test ID: " + TEST_ID);
            console.log("Created Test-data...");
            done();
        }).catch(function (error) {
            throw error;
        });
    });

});

after("Delete Test-lejlighed", function () {
    Apartments.remove({"_id": TEST_ID.toString()}, function (error) {
        if(error) {
            throw error;
        } else {
            console.log("Remove test data...");
        }
    });
});

/// Accept Tests for Test-ID 3 (User-Story 22)
/// User-Story omkring simulering af lejlighed og data-generering

// Test for acceptkriterie 3 og 4
describe("Test om hvis den oprettede lejlighed findes og opdater dens TCP", function () {

    // Her tjekker vi om hvis den kan automatisk oprette sig selv på databasen.
    it("Find '_ACCEPT_TEST_' i databases", function () {
        return Apartments.findOne({"_id": TEST_ID }, function (error, doc) {
            doc.occupants.length.should.be.greaterThanOrEqual(1);
            doc.occupants[0].name.should.be.equal("Test-Manson");
        });
    });

    it("Updater TCP information", function () {
        var apartment = controller.UpdateTCP(TEST_ID.toString(), "255.255.255.255", "5555");

        if(apartment !== undefined) {

            var tcp = apartment.tcp_address;

            tcp.ip.should.be.equal("255.255.255.255");
            tcp.port.should.be.equal("5555");

            return true;

        } else {
            return false;
        }
    });

});


// Test for acceptkriterie 1 og 2
describe("Get apartment data", function () {

    // TODO: Timeout is not working and launching the test BEFORE data has been saved locally. Tested outside of this, and it works.
    // it("get('/data'", function () {
    //     this.timeout(6000);
    //
    //     return request(app)
    //         .get("/data")
    //         .expect(200)
    //         .expect("Content-Type", /json/)
    //         .then(function (res) {
    //             res.body.length.should.be.equal(1);
    //             res.body[0].sensorID.should.be.equal("sens_id_1");
    //             res.body[0].type.should.be.equal("Temperature");
    //             res.body[0].data.length.should.be.greaterThanOrEqual(1);
    //             res.body[0].data[0].should.be.greaterThanOrEqual(0);
    //             res.body[0].data[0].should.be.lessThanOrEqual(100);
    //         });
    // });

});